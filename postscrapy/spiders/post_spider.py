import scrapy 
import re

class PostsSpider(scrapy.Spider):
    name = "posts"

    start_urls = [
        'https://www.amazon.com/Amazon-Brand-Goodthreads-Roll-Sleeve-Charcoal/dp/B07N4N26SV/ref=sr_1_10?crid=3V1P1OU4X9WXV&dchild=1&keywords=woman+cotton+shirts&qid=1591077377&sprefix=woman+cott%2Caps%2C388&sr=8-10',
    ]


    def parse(self, response):
        items = {}
        des = []
    
        title = response.css('#productTitle::text').extract()[0]
        title = re.sub(r'(\s+|\n)', ' ', title)
        price = response.css('#priceblock_ourprice::text').extract()[0]
        images = response.css('#variation_color_name img::attr(src)').extract()[0:]
        for i in response.css('#feature-bullets'):
            data = i.css('.a-list-item::text').extract()
            data = [item.replace("\n", "").replace("\t", "") for item in data]
            des.append(data)


        items['title'] = title
        items['price'] = price
        items['description'] = des
        items['img_urls'] = images
        

        # yield or give the scraped info to scrapy
        yield items